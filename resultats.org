#+TITLE:  Résultats de la consultation sur le changement de nom
#+AUTHOR: VP-SI
#+EMAIL:  vpsi@univ-paris13.fr
#+LANGUAGE:  fr
#+MACRO: paraphe VP-SI
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport

# REVEALJS
# #+INCLUDE: includes/revealpresentation.org
#+OPTIONS: reveal_center:nil
#+OPTIONS: reveal_progress:t reveal_history:nil reveal_control:t
#+OPTIONS: reveal_title_slide:nil
#+OPTIONS: reveal_rolling_links:t reveal_keyboard:t reveal_overview:t num:nil
#+OPTIONS: toc:nil reveal_global_header:t
#+REVEAL_EXTRA_CSS: /entete.css
# +REVEAL_EXTRA_CSS: https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css

# #+INCLUDE: include-menu.org

#+REVEAL_TRANS: fade
#+REVEAL_THEME: white
#+REVEAL_HLEVEL: 2
#+REVEAL_HEAD_PREAMBLE: <meta name="description" content="Feuille de route de la DSI de l'université Paris 13">
#+REVEAL_POSTAMBLE: <p>DSI Paris 13</p>
#+REVEAL_PLUGINS: (markdown notes)
# #+REVEAL_ROOT: file:///d:/reveal.js
# +REVEAL_ROOT: https://cdn.jsdelivr.net/reveal.js/3.0.0/
#+REVEAL_ROOT: /js/reveal.js-3.8.0/
#+OPTIONS: reveal_width:1000 reveal_height:900
# +OPTIONS: reveal_width:1000 reveal_height:600

# HTML
#+INFOJS_OPT: view:nil toc:nil ltoc:t mouse:underline buttons:0 path:http://orgmode.org/org-info.js
#+LINK_UP:
#+LINK_HOME:
#+XSLT:

# * Feuille de route de la DSI

#+begin_export html
<script src="https://twemoji.maxcdn.com/2/twemoji.min.js?12.0.0"></script>
<script>
document.addEventListener("DOMContentLoaded", function(event) {
  twemoji.parse(document.body);
});
</script>
<style type="text/css">
   img.emoji, .reveal section img.emoji {
   height: 1em;
   width: 1em;
   margin: 0 .05em 0 .1em;
   vertical-align: -0.1em;
   border: none;
   box-shadow: none;
   background-color: rgba(255, 255, 255, 0); /* en cas de thème avec background */
}
p { text-align: left; }
</style>
<script src="//d3js.org/d3.v3.min.js"></script>
<style type="text/css">
.bullet { font: 10px sans-serif; }
.bullet .marker { stroke: #000; stroke-width: 2px; }
.bullet .tick line { stroke: #666; stroke-width: .5px; }
.bullet .range.s0 { fill: #eee; }
.bullet .range.s1 { fill: #ddd; }
.bullet .range.s2 { fill: #ccc; }
.bullet .measure.s0 { fill: grey; }
.bullet .measure.s1 { fill: orange; }
.bullet .measure.s2 { fill: limegreen; }
.bullet .measure.s3 { fill: darkgreen; }
.legende { font-size: 14px; }
.legende .s0 { background: grey; }
.legende .s1 { background: orange; }
.legende .s2 { background: limegreen; }
.legende .s3 { background: darkgreen; color: white; }
.bullet .title { font-size: 14px; font-weight: bold; }
.bullet .subtitle { fill: #999; }
.synthese_cartouche {
  display: flex;
  justify-content: space-between;
}
.legende {
  display: flex;
//  justify-content: space-between;
  flex-direction: column;
//  justify-content: flex-start;
 align-items: flex-start;
}
.synthese_cartouche button {
  color: #0099CC;
  background: transparent;
  border: 2px solid #0099CC;
  border-radius: 3px;
  padding: 4px 8px;
  text-align: center;
  display: inline-block;
  font-size: 14px;
  margin: 2px 1px;
  -webkit-transition-duration: 0.4s; /* Safari */
  transition-duration: 0.4s;
  cursor: pointer;
  text-decoration: none;
  text-transform: uppercase;
}

/*button on hover*/
.synthese_cartouche button:hover {
  background-color: #008CBA;
  color: white;
}
</style>
<script src="bullet.js"></script>
<script src="graph_resultat.js"></script>

#+end_export

* Consultation sur le changement de nom de l'université
:PROPERTIES:
:CUSTOM_ID: consult
:END:


*Université Sorbonne Paris Nord* (USPN) est le meilleur choix, avec la mention majoritaire /me convient bien/

#+begin_export html
<div class="synthese"></div>

<div class="bullet synthese_cartouche">
<div class="legende">
<div><span class="s3">&nbsp;&nbsp;&nbsp;&nbsp;</span> convient bien</div>
<div><span class="s2">&nbsp;&nbsp;&nbsp;&nbsp;</span> convient</div>
<div><span class="s1">&nbsp;&nbsp;&nbsp;&nbsp;</span> convient modérément</div>
<div><span class="s0">&nbsp;&nbsp;&nbsp;&nbsp;</span> ne souhaite pas se prononcer</div>
</div>
<div>
<button class="button_tous">Ensemble</button>
<button class="button_students">Étudiant·es</button>
<button class="button_staff">Personnels</button>
</div>
</div>
#+end_export

/Poursuivre la lecture sur l'écran suivant 👉/

** Contexte
Du 17 au 30 septembre 2019, notre communauté universitaire a été consultée pour un changement de nom de l'université. Les trois noms proposés étaient :
- « Université de Paris Nord » (UPN)
- « Université Grand Paris Nord » (UGPN)
- « Université Sorbonne Paris Nord » (USPN)

** Organisation du scrutin
- Le scrutin a pris la forme de quatre votes électroniques, à l'aide du logiciel [[http://www.belenios.org/][Belenios]] sur un serveur de la DSI
- Le scrutin a été dépouillé au [[https://fr.wikipedia.org/wiki/Jugement_majoritaire][jugement majoritaire]]
- Deux clés, détenues par le délégué à la protection des données et le vice-président systèmes d'information, étaient nécessaires pour dépouiller
- 2978 personnes ont participé au vote. Les deux collèges, personnels (555 votes exprimés) et étudiant·es (2423 votes exprimés), ont été ramenés à un même poids au dépouillement.
- Les résultats sont dans l'écran suivant 👉, les écrans ci-dessous 👇 précisent l'organisation
*** Jugement majoritaire
- Le jugement majoritaire est un mode de scrutin où l'on exprime son avis sur chacune des propositions, selon une échelle de mentions choisie en fonction de la question. Ainsi, *on exprime ce qu'on pense plutôt que d'être enclin à un calcul stratégique.*
- Le dépouillement se fait en attribuant à chaque proposition sa mention médiane, et en sélectionnant la meilleure. Cela signifie que *le jugement majoritaire favorise les propositions qui satisfont au mieux plus de 50% des personnes*.


*** Détails des votes électroniques
- [[https://dsi.univ-paris13.fr/belenios/elections/LET3k48Jyr7BLX/][Premier vote]] du 17 sept. au 30 sept. auprès des personnels, codes de vote gérés par le serveur, authentification ENT (CAS),  544 bulletins.
- [[https://dsi.univ-paris13.fr/belenios/elections/efu75xHe5jDvEn/][Deuxième vote]]  du 20 sept. au 30 sept. auprès des étudiant·es inscrit·es ou pré-inscrit·es pour l'année 2019-2020, codes de vote manuels et CAS, 2294 bulletins.
- [[https://dsi.univ-paris13.fr/belenios/elections/d5ZcRbdQ7che2t/][Troisième vote]] du 27 sept. au 30 sept. étudiant·es inscrit·es au cours de la semaine, codes de vote manuels et CAS, 129 bulletins.
- Quatrième vote, celui des professeur·es émérites et de quelques personnels, 11 bulletins.
*** Questions posées
Pour chacun des trois noms, il était demandé à chaque personne si le nom :
1. « me convient bien »
2. « me convient »
3. « me convient modérément »
4. « je ne souhaite pas me prononcer »
Il était précisé que cette dernière mention (4) serait la plus négative au cours du dépouillement. Il fallait accorder l'une de ces quatre mentions (et une seule) à chaque proposition de nom pour que le vote soit valide.

*** Déroulement du vote
- Durant le scrutin, 4 personnes se sont retrouvées sans solution pour voter (code non reçu, autre erreur inconnue), soit 0,13% des votes exprimés.
- Le dépouillement des votes a été effectué le mardi 1er octobre par le DPD et le VP-SI. Le serveur a ensuite été arrêté pour laisser le temps de communiquer sur les résultats.
- Pour laisser à chacun la possibilité de vérifier le comptage des votes et la bonne prise en compte de chaque vote exprimé (via les outils [[http://www.belenios.org/][Belenios]]), le serveur sera relancé le 4 octobre.
- Les données détaillées du vote seront effacées le 15 octobre et le serveur arrêté.
*** Dépouillement
- Le dépouillement a eu lieu selon les règles du [[https://fr.wikipedia.org/wiki/Jugement_majoritaire][jugement majoritaire]], il n'a pas été nécessaire de départager des ex-aequo.
- Les bulletins des personnels ont étés pondérés de façon à ce que l'ensemble des bulletins des personnels compte autant que ceux des étudiants, plus nombreux. Le facteur de pondération de 4,3 a été choisi comme une approximation du ratio de votes exprimés entre les deux collèges. Ainsi chaque bulletin de personnel a compté 4,3 fois plus qu'un bulletin étudiant.
** Résultats
- Le résultat du dépouillement donne  *Université Sorbonne Paris Nord* en tête avec la mention majoritaire /convient bien/. Cette proposition recueille 68% de satisfaction (convient bien et convient).
- En deuxième position vient « Université de Paris Nord », avec la mention majoritaire /convient modérément/ et satisfaisant 43% des votant·es.
- En dernier vient « Université Grand Paris Nord », avec la mention majoritaire /convient modérément/.
- Une synthèse graphique est donnée en [[#consult][première page]].

*** Vote des personnels

| Nom  | bien | convient | modérément | NSPP |
|------+------+----------+------------+------|
| UPN  |  147 |      129 |        205 |   74 |
| UGPN |   69 |       69 |        301 |  116 |
| USPN |  220 |       84 |        157 |   94 |

En pourcentage :

| Nom  | bien | convient | modérément | NSPP |
|------+------+----------+------------+------|
| UPN  |  26% |      23% |        37% |  13% |
| UGPN |  12% |      12% |        54% |  21% |
| USPN |  40% |      15% |        28% |  17% |

*** Vote des étudiant·es

| Nom  | bien | convient | modérément | NSPP |
|------+------+----------+------------+------|
| UPN  |  363 |      480 |       1229 |  351 |
| UGPN |  229 |      400 |       1321 |  473 |
| USPN | 1740 |      238 |        340 |  105 |

En pourcentage :
| Nom  | bien | convient | modérément | NSPP |
|------+------+----------+------------+------|
| UPN  |  15% |      20% |        51% |  14% |
| UGPN |   9% |      17% |        55% |  20% |
| USPN |  72% |      10% |        14% |   4% |

*** Totaux
- Le total des votes (avec pondération de 4.3 pour les personnels) donne en pourcentages les résultats suivants :

|  Nom | bien | convient | modérément | NSPP |
|------+------+----------+------------+-----|
|  UPN |  21% |      22% |        44% | 14% |
| UGPN |  11% |      14% |        54% | 20% |
| USPN |  56% |      12% |        21% | 11% |

- Les mentions majoritaires sont /modérément/ pour /Université de Paris Nord/ et /Université Grand Paris Nord/ et /bien/ pour /Université Sorbonne Paris Nord/ qui est donc le choix préféré.


*** Départage des ex-aequo pour la seconde place :noexport:
- En second choix derrière /Sorbonne Paris Nord/ vient /de Paris Nord/ qui est le plus près de changer de mention (distance de 7%)  pour passer à /convient/, une mention plus positive. Le tableau suivant montre la distance à la médiane des changements de mentions (le signe +/- dénote le côté du changement).

| Nom  | bien-convient | convient-modéré | modéré-pas |
|------+---------------+-----------------+------------|
| UPN  |           29% | *7%*            |       -36% |
| UGPN |           39% | 24%             |       -29% |

* Référencement :noexport:

 - Actuellement une recherche sur les termes[[https://www.qwant.com/?q=université%20Sorbonne%20Paris%20Nord&t=web][ Université Sorbonne Paris Nord]] sur Qwant, donne en premier des pages institutionnelles de notre université.

 - Les noms de domaines de premier niveau univ-spn.fr et uspn.fr on été déposés par l'université.
