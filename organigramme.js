/*
   background-color: #153a23;
   color: #005031;
   color: #113321;
*/

function stylishmapping(d) {
    const width = Math.round(Math.random()*50+300);
    const height = Math.round(Math.random()*20+130);
    const cornerShape = 'CIRCLE'; // ['ORIGINAL','ROUNDED','CIRCLE'][Math.round(Math.random()*2)];
    const nodeImageWidth = 100;
    const nodeImageHeight = 100;
    const centerTopDistance = 0;
    const centerLeftDistance = 0;
    const expanded = d.expanded ? true : false; //d.id=="O-6"
    
    const  titleMarginLeft = nodeImageWidth/2+20+centerLeftDistance
    const  contentMarginLeft = width/2+25

    var bgcolor = {  // background color of node card
        red:0x15,
        green:0x3a,
        blue:0x23,
        alpha:1,
      };

    if (d.nom == "vacant") {
        bgcolor = {  // background color of node card
            red:0x1e,
            green:0x82,
            blue:0x4c,
            alpha:1,
          };
    }

    return {
      nodeId:d.id,    // node identificator
      parentNodeId:d.parentId,  // parent node identificator
      width:width,   // node card width
      height:height,  // node card height
      borderWidth:1,  // node card border width
      borderRadius:15,  // node card border radius 
      borderColor:{    // border color
          red:0x11,
          green:0x33,
          blue:0x21,
          alpha:1,
      },
      backgroundColor: bgcolor,
      nodeIcon: {
          icon: "tree.png",
          size: 30
      },
      nodeImage:{        // node image properties
        url:d.imageUrl,    // url to image
        width:nodeImageWidth,   // image width
        height:nodeImageHeight,   // image height
        centerTopDistance:centerTopDistance,   // vertical distance from center
        centerLeftDistance:centerLeftDistance,  // horizontal distance from node center
        cornerShape:cornerShape,      // corner shape, can be 'ORIGINAL','ROUNDED','CIRCLE'
        shadow:false,             // if image has shadow (if yes, performance may be poor for 50+ visible nodes)
        borderWidth:0,           // image border width
        borderColor:{           // image border color
            red:0x11,
            green:0x33,
            blue:0x21,
            alpha:1,
        }
      },    
      // node card content
      template:`<div>           
                    <div style="margin-left:${titleMarginLeft}px;
                                margin-top:10px;
                                font-size:20px;
                                font-weight:bold;
                           ">${d.nom} </div>
                   <div style="margin-left:${titleMarginLeft}px;
                                margin-top:3px;
                                font-size:16px;
                           ">${d.titre} </div>
  
                   <div style="margin-left:${titleMarginLeft}px;
                                margin-top:3px;
                                font-size:14px;
                           ">${d.pole.value}</div>
  
                   <div style="margin-left:${contentMarginLeft}px;
                               margin-top:15px;
                               font-size:13px;
                               bottom:5px;
                              ">
                        <div>${d.lieu}</div>
                        <div style="margin-top:5px">${d.affectation}</div>
                   </div>
                </div>`,
      connectorLineColor:{      // Edge color
            red:0x11,
            green:0x33,
            blue:0x21,
            alpha:1
      },
      connectorLineWidth:5,      // Edge width
      dashArray:'',              // We can have stripped edges, you should specify dashArray for it (google: svg dashArray)
      expanded:expanded          // If we want node to be expanded
    } 
}
