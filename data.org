#+TITLE:  Données publiques de l'université Paris 13
#+AUTHOR: VP-SI et ensemble de la DSI
#+EMAIL:  vpsi@univ-paris13.fr
#+LANGUAGE:  fr
#+MACRO: paraphe VP-SI
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport

# REVEALJS
# #+INCLUDE: includes/revealpresentation.org
#+OPTIONS: reveal_center:nil reveal_progress:t reveal_history:nil reveal_control:t
#+OPTIONS: reveal_title_slide:nil
#+OPTIONS: reveal_rolling_links:t reveal_keyboard:t reveal_overview:t num:nil
#+OPTIONS: toc:nil reveal_global_header:t
#+REVEAL_EXTRA_CSS: entete.css
# +REVEAL_EXTRA_CSS: https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css

#+INCLUDE: include-menu.org

#+REVEAL_TRANS: fade
#+REVEAL_THEME: white
#+REVEAL_HLEVEL: 3
#+REVEAL_HEAD_PREAMBLE: <meta name="description" content="Feuille de route de la DSI de l'université Paris 13">
#+REVEAL_POSTAMBLE: <p>DSI Paris 13</p>
#+REVEAL_PLUGINS: (markdown notes)
# #+REVEAL_ROOT: file:///d:/reveal.js
#+REVEAL_ROOT: https://cdn.jsdelivr.net/reveal.js/3.0.0/
# +REVEAL_ROOT: /js/reveal.js-3.8.0/
#+OPTIONS: reveal_width:1000 reveal_height:900
# +OPTIONS: reveal_width:1000 reveal_height:600

# HTML
#+INFOJS_OPT: view:nil toc:nil ltoc:t mouse:underline buttons:0 path:http://orgmode.org/org-info.js
#+LINK_UP:
#+LINK_HOME:
#+XSLT:

# * Feuille de route de la DSI

#+begin_export html
<script src="https://twemoji.maxcdn.com/2/twemoji.min.js?12.0.0"></script>
<script>
document.addEventListener("DOMContentLoaded", function(event) {
  twemoji.parse(document.body);
});
</script>
<style type="text/css">
   img.emoji, .reveal section img.emoji {
   height: 1em;
   width: 1em;
   margin: 0 .05em 0 .1em;
   vertical-align: -0.1em;
   border: none;
   box-shadow: none;
   background-color: rgba(255, 255, 255, 0); /* en cas de thème avec background */
}
</style>
#+end_export

* Données publiques
:PROPERTIES:
:CUSTOM_ID: data
:END:
- L'université Paris 13 publie des données de scolarité en /open data/ sur [[https://www.data.gouv.fr/fr/organizations/universite-paris-13/][data.gouv.fr]].
#+begin_export html :noexport:
<div data-udata-dataset="58e34f7dc751df5d2777388c"></div>
<script data-udata="https://www.data.gouv.fr/" src="https://static.data.gouv.fr/static/oembed.js" async defer></script>
#+end_export
- Ces données sont exploitées pour proposer une visualisation de nos parcours d'études par étapes de diplôme. Si les codes étapes vous parlent, c'est [[./viz/traces/][par ici]].
- Les campus de l'université sont également [[https://www.openstreetmap.org/relation/9973873][représentés]] sur les cartes collaboratives OpensStreetMap. N'hésitez pas à rejoindre la communauté OpenStreetMap pour améliorer et mettre à jour ces données.
